.. pycf documentation master file, created by
   sphinx-quickstart on Sat Oct 12 13:01:26 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pycf's documentation
===============================

.. toctree::
  :maxdepth: 2

  overview
  install
  pyemp
  spinh
  reference
  gpc_installation
  emp_lapack
  license

