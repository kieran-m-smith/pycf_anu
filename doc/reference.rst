==============
pycf reference
==============

:mod:`cfl` -- python wrapper for cfl
======================================
.. autoclass:: cfl.StateLabels
  :members:

.. autoclass:: cfl.Tensor
  :members:

.. autoclass:: cfl.Hamiltonian
  :members:

.. autoclass:: cfl.SpinHamiltonian
  :members:

.. autofunction:: cfl.sh_svd

.. autofunction:: cfl.zeeman_sh_coeff

.. autofunction:: cfl.hyperfine_sh_coeff

.. autofunction:: cfl.quadrupole_sh_coeff

.. autoclass:: cfl.ExData
  :members:

.. autoclass:: cfl.EFitRunner
  :members:

.. autoclass:: cfl.MHFitRunner
  :members:

.. autoclass:: cfl.ESHFitRunner
  :members:

.. autoclass:: cfl.MESHFitRunner
  :members:

.. autoclass:: cfl.CFLMin
  :members:

.. autofunction:: cfl.e_fit

.. autofunction:: cfl.mh_fit

.. autofunction:: cfl.esh_fit

.. autofunction:: cfl.mesh_fit

.. autoclass:: cfl.ZEFOZSearchRunner
  :members:

.. autofunction:: cfl.zefoz


:mod:`import_sljm` -- parse emp sljm matrix elements
====================================================

.. automodule:: import_sljm
  :members:



:mod:`cfl_util` -- utility functions for :mod:`cfl`
==================================================

.. automodule:: cfl_util
  :members:


:mod:`pyemp` -- emp python wrapper
==================================

.. automodule:: pyemp
  :members:

:mod:`spinh` -- spin Hamiltonian calculations
=============================================

.. automodule:: spinh
  :members:

:mod:`matel` -- Matrix element function
=======================================

.. automodule:: matel
  :members:

:mod:`njsymbols` -- njsymbol functions
======================================

.. automodule:: njsymbols
  :members:
